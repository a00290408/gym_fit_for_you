package com.ait.customer;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mockStatic;

import org.junit.Before;
import org.junit.Test;
import org.mockito.MockedStatic;

public class LoginBeanTest {
	private LoginBean loginBean;

	@Before
	public void setUp() {
		loginBean = new LoginBean();
	}

	@Test
	public void testSetEmail() {
		loginBean.setEmail("abc@gamil.com");
		assertEquals("abc@gamil.com", loginBean.getEmail());

	}

	@Test
	public void testSetEmail2() {
		loginBean.setEmail("");
		assertEquals("", loginBean.getEmail());

	}
	@Test
	public void testSetEmail3() {
		loginBean.setEmail("abc@watersidegym.com");
		assertEquals("abc@watersidegym.com", loginBean.getEmail());

	}
	
	@Test
	public void testSetPassword() {
		loginBean.setPassword("pass@123");
		assertEquals("pass@123", loginBean.getPassword());
	}

	@Test
	public void testChangePassword() {
		loginBean.setPassword("pass@123");
		assertEquals("pass@123", loginBean.getPassword());
		loginBean.setPassword("pass@1234");
		assertEquals("pass@1234", loginBean.getPassword());
	}

	@Test
	public void testChangeEmail() {
		loginBean.setEmail("abc@gamil.com");
		assertEquals("abc@gamil.com", loginBean.getEmail());
		loginBean.setEmail("abc1@gamil.com");
		assertEquals("abc1@gamil.com", loginBean.getEmail());
	}

	@Test
	public void testValidateCustomerLoginValid() {
		loginBean.setEmail("John@gmail.com");
		loginBean.setPassword("Johnny");
		CustomerData data = new CustomerData();
		data.init();
		try (MockedStatic<Helper> mocked = mockStatic(Helper.class)) {
			mocked.when(() -> Helper.getBean("customerData", CustomerData.class)).thenReturn(data);
			assertEquals("CustomerDashboard.xhtml", loginBean.validateCustomerLogin());
		}
	}
	@Test
	public void testValidateCustomerLoginValid2() {
		loginBean.setEmail("John@watersidegym.com");
		loginBean.setPassword("Johnny");
		CustomerData data = new CustomerData();
		data.init();
		try (MockedStatic<Helper> mocked = mockStatic(Helper.class)) {
			mocked.when(() -> Helper.getBean("customerData", CustomerData.class)).thenReturn(data);
			assertEquals("TrainerDashboard.xhtml", loginBean.validateCustomerLogin());
		}
	}
	@Test
	public void testlogoutUser() {
		assertEquals("index.xhtml",loginBean.logoutUser());
	}
	
	@Test
	public void testisLoggedIn() {
		loginBean.setLoggedIn(true);
		assertEquals(true,loginBean.isLoggedIn());
	}
	
	@Test
	public void testisLoggedInAsPT() {
		loginBean.setLoggedInAsPT(true);
		assertEquals(true,loginBean.isLoggedInAsPT());
	}
	
	@Test
	public void testGetEmail2() {
		loginBean.setEmail2("1");
		assertEquals("1",loginBean.getEmail2());
	}
	
	@Test
	public void testisMember() {
		loginBean.setMember(true);
		assertEquals(true,loginBean.isMember());
	}
	
}
