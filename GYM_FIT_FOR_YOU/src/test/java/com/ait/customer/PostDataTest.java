package com.ait.customer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.time.LocalDateTime;

import org.junit.Before;
import org.junit.Test;

public class PostDataTest {
	PostData postData = new PostData();

	@Before
	public void setUp() {
		postData = new PostData();
		postData.init();
	}

	@Test
	public void testGetTitle() {
		postData.setTitle("NewTitle");
		assertEquals("NewTitle", postData.getTitle());
	}

	@Test
	public void testGetContent() {
		postData.setContent("SomeContent");
		assertEquals("SomeContent", postData.getContent());
	}

	@Test
	public void testGetAuthor() {
		postData.setAuthor(LoginBean.email2);
		assertEquals(LoginBean.email2, postData.getAuthor());
	}
	
	@Test
	public void testGetMedia() {
		postData.setMedia("abc@gamil.com");
		assertEquals("abc@gamil.com", postData.getMedia());
	}
	
	@Test
	public void testGetPosts() {
		postData.setPosts(null);
		assertEquals(null, postData.getPosts());
	}
	@Test
	public void testGetImageName() {
		postData.setImageName("abc@gamil.com");
		assertEquals("abc@gamil.com", postData.getImageName());
	}
	@Test
	public void testIsImage() {
		postData.setImage(true);
		assertEquals(true, postData.isImage());
	}
	
	@Test
	public void testIsMembersOnly() {
		postData.setMembersOnly(true);
		assertEquals(true, postData.isMembersOnly());
	}

	@Test
	public void testGetDate() {
		postData.setDate("01");
		LocalDateTime now = LocalDateTime.now();
		int min = now.getMinute();
		int hour = now.getHour();
		int day = now.getDayOfMonth();
		int month = now.getMonthValue();
		int year = now.getYear();
		String date = day + "/" + month + "/" + year + " " + String.format("%02d", hour) + ":"
				+ String.format("%02d", min);
		assertEquals(date, postData.getDate());
	}

	@Test
	public void testInit() {
		assertEquals(3, postData.getPosts().size());
		assertEquals("Portable Home Kits", postData.getPosts().get(0).getTitle());
		assertEquals("Gyming During Covid", postData.getPosts().get(1).getTitle());
		assertEquals("Why You Need a Personal Trainer", postData.getPosts().get(2).getTitle());
	}

	@Test
	public void testEditPost() {
		postData.editPost(postData.getPosts().get(0));
		assertTrue(postData.getPosts().get(0).isCanEdit());
	}

	@Test
	public void testDeletePost() {
		postData.deletePost(postData.getPosts().get(0));
		assertEquals(2, postData.getPosts().size());
	}

	@Test
	public void testSaveAction() {
		postData.editPost(postData.getPosts().get(0));
		postData.saveAction();
		assertFalse(postData.getPosts().get(0).isCanEdit());
	}

	/*@Test
	public void testAddPost() {
		postData.setTitle("Jack");
		postData.setContent("Flynn");
		postData.setAuthor("Galway");
		postData.setDate("0851558023");
		postData.addPost();
		assertEquals(4, postData.getPosts().size());
	}*/
}
