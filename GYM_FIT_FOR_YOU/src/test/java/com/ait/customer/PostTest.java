package com.ait.customer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class PostTest {
private Post post;
private Post post2;
	
	@Before
	public void setUp() {
		post = new Post("FirstPost", "class update", "Cormac", "02-08-2021", null, false, false);
		post2 = new Post("FirstPost", "class update", "Cormac", "02-08-2021", null, "classname", false);
	}

	@Test
	public void testConstructor() {	
		assertEquals("FirstPost", post.getTitle());
		assertEquals("class update", post.getContent());
		assertEquals("Cormac", post.getAuthor());
		assertEquals("02-08-2021", post.getDate());
	}
	
	@Test
	public void testTitleChange() {
		post.setTitle("SecondPost");
		assertEquals("SecondPost", post.getTitle());
	}
	
	@Test
	public void testContentChange() {
		post.setContent("class updated again");
		assertEquals("class updated again", post.getContent());
	}
	
	@Test
	public void testAuthorChange() {
		post.setAuthor("Not Cormac");
		assertEquals("Not Cormac", post.getAuthor());
	}
	
	@Test
	public void testDateChange() {
		post.setDate("03-08-2021");
		assertEquals("03-08-2021", post.getDate());
	}
	
	@Test
	public void testAllowingEditing() {
		post.setCanEdit(true);
		assertTrue(post.isCanEdit());
	}

	@Test
	public void testmediaChange() {
		post.setmedia("Not Cormac");
		assertEquals("Not Cormac", post.getmedia());
	}
	
	@Test
	public void testClassNameChange() {
		post.setClassName("Not Cormac");
		assertEquals("Not Cormac", post.getClassName());
	}
	
	@Test
	public void testImageChange() {
		post.setImage(true);
		assertEquals(true, post.isImage());
	}
	
	@Test
	public void testMembersOnlyChange() {
		post.setMembersOnly(true);
		assertEquals(true, post.isMembersOnly());
	}
	
	@Test
	public void testToString() {
		assertEquals("Post [title=FirstPost, content=class update, author=Cormac, date=02-08-2021, canEdit=false, media=null, className=, image=false]", post.toString());
	}
}