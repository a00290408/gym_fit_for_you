package com.ait.customer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class ClassesTest {
private Classes classes;
private Classes classes2;
	@Before
	public void setUp() {
		//classes = new Classes("FirstClasses", "class update", "Cormac", "02-08-2021", null, false, false);
		classes = new Classes("Aerobics", "This class is ideal for those looking to get into aerobics",
				"Jack Flynn", "30/07/2021 02:25", 20, "",true, null);
		classes2 = new Classes("Aerobics", "This class is ideal for those looking to get into aerobics",
				"Jack Flynn", "30/07/2021 02:25", 0, "",true, null);
	}

	@Test
	public void testConstructor() {	
		assertEquals("Aerobics", classes.getClassName());
		assertEquals("This class is ideal for those looking to get into aerobics", classes.getInformation());
		assertEquals("", classes.getParticipant());
		assertEquals("Jack Flynn", classes.getTrainer());
		assertEquals("30/07/2021 02:25", classes.getDateTime());
	}
	
	@Test
	public void testClassNameChange() {
		classes.setClassName("SecondClasses");
		assertEquals("SecondClasses", classes.getClassName());
	}
	
	@Test
	public void testIndividualCustomer() {
		classes.doNothing();
		classes.setIndividualCustomer("SecondClasses");
		assertEquals("SecondClasses", classes.getIndividualCustomer());
	}
	
	@Test
	public void testInformationChange() {
		classes.setInformation("class updated again");
		assertEquals("class updated again", classes.getInformation());
	}
	
	@Test
	public void testTrainerChange() {
		classes.setTrainer("Not Cormac");
		assertEquals("Not Cormac", classes.getTrainer());
	}
	
	@Test
	public void testDateTimeChange() {
		classes.setDateTime("03-08-2021");
		assertEquals("03-08-2021", classes.getDateTime());
	}
	@Test
	public void testParticipantChange() {
		classes.setParticipant("03-08-2021");
		assertEquals("03-08-2021", classes.getParticipant());
	}
	@Test
	public void testAddParticipant() {
		classes.addParticipant();
		assertEquals("abc@gamil.com, ", classes.getParticipant());
	}
	
	@Test
	public void testAllowingEditing() {
		classes.setCanEdit(true);
		assertTrue(classes.isCanEdit());
	}
	@Test
	public void testAlreadyBooked() {
		classes.setAlreadyBooked(true);
		assertTrue(classes.isAlreadyBooked());
	}
	
	@Test
	public void testTimeConflict() {
		classes.setTimeConflict(true);
		assertTrue(classes.isTimeConflict());
	}
	@Test
	public void testIsIndividualProgram() {
		classes.setIndividualProgram(true);
		assertEquals(true, classes.isIndividualProgram());
	}
	
	@Test
	public void testCapacityChange() {
		classes.setCapacity(1);
		assertEquals(1, classes.getCapacity());
	}
	
	@Test
	public void testCanAddChange() {
		classes.setCanAdd(true);
		assertEquals(true, classes.isCanAdd());
	}
	@Test
	public void testSpacesAvailableChange() {
		classes.setSpacesAvailable(true);
		assertEquals(true, classes.isSpacesAvailable());
	}
	@Test 
	public void testAddCustomer() {
		Customer customerOne = new Customer("David", "Smith", "Athlone", "08912345", "David@gmail.com", "Johnny");
		classes.addCustomer(customerOne);
		assertTrue(classes.getCustomerList().contains(customerOne));
	}
	@Test 
	public void testRemoveCustomer() {
		
		Customer customerOne = new Customer("David", "Smith", "Athlone", "08912345", "David@gmail.com", "Johnny");
		classes.addCustomer(customerOne);
		assertTrue(classes.getCustomerList().contains(customerOne));
		int size = classes.getCustomerList().size();
		classes.removeCustomer(customerOne);
		assertEquals((size-1),classes.getCustomerList().size());
	}
	
	@Test 
	public void testparticipantAdded() {
		classes.participantAdded();
		assertEquals(true, classes.isAlreadyBooked());
	}
	@Test 
	public void testToString() {
		assertEquals("Classes [className=Aerobics, information=This class is ideal for those looking to get into aerobics, trainer=Jack Flynn, dateTime=30/07/2021 02:25, canEdit=false, canAdd=false, capacity=20, spacesAvailable=true, alreadyBooked=false, timeConflict=false, participant=]", classes.toString());
	}

	@Test
	public void testremovePlace() {
		int cap = classes2.getCapacity();
		classes2.removePlace();
		assertEquals((cap-1), classes2.getCapacity());
		
	}

}