package com.ait.customer;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class UploadManagedBeanTest {
private UploadManagedBean uploadManagedBean;
	
	@Before
	public void setUp() {
		uploadManagedBean = new UploadManagedBean();
	}
	
	@Test
	public void testGetMedia() {
		uploadManagedBean.setMedia("SecondPost");
		assertEquals("SecondPost", uploadManagedBean.getMedia());
	}

	@Test
	public void testIsImage() {
		uploadManagedBean.setImage(true);
		assertEquals(true, uploadManagedBean.isImage());
	}


}
