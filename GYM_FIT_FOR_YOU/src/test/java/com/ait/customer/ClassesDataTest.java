package com.ait.customer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;

public class ClassesDataTest {
	ClassesData classesData = new ClassesData();
	Classes classes;
	@Before
	public void setUp() {
		classesData = new ClassesData();
		classes = new Classes("Aerobics", "This class is ideal for those looking to get into aerobics",
				"Jack Flynn", "30/07/2021 02:25", 20, "",true, null);
		classesData.init();
	}
	
	@Test
	public void testGetClassName() {
		classesData.setClassName("Jack");
		assertEquals("Jack", classesData.getClassName());
	}
	
	@Test
	public void testGetInformation() {
		classesData.setInformation("Flynn");
		assertEquals("Flynn", classesData.getInformation());
	}
	
	@Test
	public void testGetParticipant() {
		classesData.setParticipant("Galway");
		assertEquals("Galway", classesData.getParticipant());
	}
	
	@Test
	public void testGetTrainer() {
		classesData.setTrainer("0851558023");
		assertEquals("0851558023", classesData.getTrainer());
	}
	
	@Test
	public void testGetDateTime() {
		classesData.setDateTime("Jack@gmail.com");
		assertEquals("Jack@gmail.com", classesData.getDateTime());
	}
	
	@Test
	public void testIsIndividualProgram() {
		classesData.setIndividualProgram(true);
		assertEquals(true, classesData.isIndividualProgram());
	}
	
	@Test
	public void testIsCanAdd() {
		classesData.setCanAdd(true);
		assertEquals(true, classesData.isCanAdd());
	}
	
	@Test
	public void testGetCapacity() {
		classesData.setCapacity(2);
		assertEquals(2, classesData.getCapacity());
	}

	@Test
	public void testInit() {
		assertEquals(6, classesData.getMyClasses().size());
		assertEquals("Aerobics", classesData.getMyClasses().get(0).getClassName());
		assertEquals("Strongman", classesData.getMyClasses().get(1).getClassName());
		assertEquals("Cardio", classesData.getMyClasses().get(2).getClassName());
		assertEquals("Boxing", classesData.getMyClasses().get(3).getClassName());
		assertEquals("Core", classesData.getMyClasses().get(4).getClassName());
		assertEquals("Zumba", classesData.getMyClasses().get(5).getClassName());
	}
	
	@Test
	public void testEditClasses() {
		classesData.editClasses(classesData.getMyClasses().get(0));
		assertTrue(classesData.getMyClasses().get(0).isCanEdit());
	}
	
	@Test
	public void testDeleteClasses() {
		classesData.deleteClasses(classesData.getMyClasses().get(0));
		assertEquals(5, classesData.getMyClasses().size());
	}
	
	@Test
	public void testSaveAction() {
		classesData.editClasses(classesData.getMyClasses().get(0));
		classesData.saveAction(classesData.getMyClasses().get(0));
		assertFalse(classesData.getMyClasses().get(0).isCanEdit());
	}
	
/*	@Test
	public void testAddClasses() {
		classesData.setClassName("Jack");
		classesData.setInformation("Flynn");
		classesData.setParticipant("Galway");
		classesData.setTrainer("0851558023");
		classesData.setDateTime("Jack@gmail.com");
		//classesData.setCapacity("galway1604");
		classesData.addClasses();
		assertEquals(7, classesData.getMyClasses().size());
	}*/
	
	@Test
	public void testIsPT() {
		assertFalse(classesData.isPT());
		LoginBean.email2="John@watersidegym.com";
		assertTrue(classesData.isPT());
	}
	
	@Test
	public void testAddCustomer() {
		classesData.addCustomer(classes);
	}
	@Test
	public void testAddParticipant() {
		assertEquals(null,classesData.addParticipant(classes));
	}
	@Test
	public void testRemoveClass() {
		//LoginBean.email2="";
		//classesData.addCustomer(classes);
		//classesData.removeClass(classes);
		//assertEquals(null,classesData.addParticipant(classes));
	}
	@Test
	public void testfindCustomerClasses() {
		assertTrue(classesData.findCustomerClasses("hello").isEmpty());
	}
	@Test
	public void testgetCustomerClasses() {
		assertTrue(classesData.getCustomerClasses().isEmpty());
	}

	@Test
	public void testsetCustomerClasses() {
		classesData.setCustomerClasses(null);
		assertEquals(null,classesData.getCustomerClasses());
	}
	@Test
	public void testsetMyClasses() {
		classesData.setMyClasses(null);
		assertEquals(null,classesData.getMyClasses());
		
	}
	@Test
	public void testAddClasses() {
		classesData.setDateTime("1111111111111");
		assertEquals("Classes",classesData.addClasses());
		
	}
	@Test
	public void testAddClasses4() {
		classesData.setDateTime("1111111111111");
		classesData.setIndividualProgram(true);
		assertEquals("Classes",classesData.addClasses());
		
		
	}
	@Test
	public void testAddClasses2() {
		classesData.setDateTime("0000000000000");
		assertEquals("Classes",classesData.addClasses());
		
	}
	@Test
	public void testAddClasses3() {
		classesData.setDateTime("1001001001001");
		assertEquals("Classes",classesData.addClasses());
		
	}

	@Test
	public void testIndividualCustomer() {
		classesData.setIndividualCustomer("1001001001001");
		assertEquals("1001001001001",classesData.getIndividualCustomer());
		
	}
	@Test
	public void testGetDateToday() {
		Date dateToday = new Date();
		assertEquals(dateToday, classesData.getDateToday());
	}
	
	@Test
	public void testRemoveIndividualProgram() {

		classesData.removeIndividualProgram("");
	}
	
	@Test
	public void testGetTotalClasses() {
		assertEquals(6,classesData.getTotalClasses());
	}
	
	@Test
	public void testGetTotalRegisteredCustomers() {
		assertEquals(0,classesData.getTotalRegisteredCustomers());
	}
}
