package com.ait.customer;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;

import javax.servlet.http.Part;

import org.junit.Before;
import org.junit.Test;

public class UploadHelperTest {
private UploadHelper uploadHelper;
Part someFilePart;
UploadManagedBean uploadBean; 

	@Before
	public void setUp() {
		uploadHelper = new UploadHelper();
		someFilePart = new Part() {
			
			@Override
			public void write(String fileName) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public String getSubmittedFileName() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public long getSize() {
				// TODO Auto-generated method stub
				return 0;
			}
			
			@Override
			public String getName() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public InputStream getInputStream() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Collection<String> getHeaders(String name) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Collection<String> getHeaderNames() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public String getHeader(String name) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public String getContentType() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public void delete() {
				// TODO Auto-generated method stub
				
			}
		};
		uploadBean = new UploadManagedBean();
	}
	
	@Test
	public void testSetFileName() {
		uploadHelper.uploadMedia.setFileName("SecondPost");
		assertEquals("SecondPost", uploadHelper.uploadMedia.getFileName());
	}

	@Test
	public void testSetImage() {
		uploadHelper.uploadMedia.setImage(true);
		assertEquals(true, uploadHelper.uploadMedia.isImage());
	}
	
	@Test
	public void testCheckFileTypeReturnFalse() {
		assertEquals(false, uploadHelper.checkFileType(""));
	}
	@Test
	public void testCheckFileTypeReturnTrue() {
		assertEquals(true, uploadHelper.checkFileType("someMedia.jpg"));
	}
	
	@Test
	public void testCheckInvalidFile() {
		assertEquals("nomedia.jpg", uploadHelper.processUpload(someFilePart).getFileName());
	}
	
	@Test
	public void testGetFileName() {
		assertEquals(null, uploadHelper.getFilename(someFilePart));
	}
	@Test
	public void testFileParams() throws IOException {
		someFilePart.getContentType();
		someFilePart.getHeader(null);
		someFilePart.getName();
		someFilePart.getSubmittedFileName();
		someFilePart.getHeader(null);
		someFilePart.getHeaderNames();
		someFilePart.getInputStream();
		someFilePart.write(null);
		someFilePart.delete();
		someFilePart.getHeaders("nomedia.jpg");
		uploadBean.setPart(someFilePart);
		uploadBean.getPart();
		assertEquals(null, uploadHelper.getFilename(someFilePart));
	}
	
}
