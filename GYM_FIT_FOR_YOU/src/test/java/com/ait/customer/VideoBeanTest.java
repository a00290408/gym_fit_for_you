package com.ait.customer;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class VideoBeanTest {
	private VideoBean videoBean;
	@Before
	public void setUp() {
		videoBean = new VideoBean();
	}

	@Test
	public void testSetEvents() {
		videoBean.setEvents("events");
		assertEquals("events", videoBean.getEvents());
	}

	@Test
	public void testOnPlay() {
		videoBean.onPlay();
		assertEquals("play\n", videoBean.getEvents());
	}

	@Test
	public void testOnPause() {
		videoBean.onPause();
		assertEquals("pause\n", videoBean.getEvents());
	}

	@Test
	public void testOnSeeking() {
		videoBean.onSeeking();
		assertEquals("seeking\n", videoBean.getEvents());
	}

	@Test
	public void testOnCanplaythrough() {
		videoBean.onCanplaythrough();
		assertEquals("can play through\n", videoBean.getEvents());
	}

	@Test
	public void testOnLoadeddata() {
		videoBean.onLoadeddata();
		assertEquals("loaded data\n", videoBean.getEvents());
	}

}
