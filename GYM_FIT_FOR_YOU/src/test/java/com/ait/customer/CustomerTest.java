package com.ait.customer;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class CustomerTest {
	private Customer customer;
	
	@Before
	public void setUp() {
		customer = new Customer("Jack", "Flynn", "Galway", "0851558023", "Jack@gmail.com", "JF1234");
	}

	@Test
	public void testConstructor() {	
		assertEquals("Jack", customer.getFirstName());
		assertEquals("Flynn", customer.getLastName());
		assertEquals("Galway", customer.getAddress());
		assertEquals("0851558023", customer.getPhoneNumber());
		assertEquals("Jack@gmail.com", customer.getEmail());
		assertEquals("JF1234", customer.getPassword());
	}
	
	@Test
	public void testFirstNameChange() {
		customer.setFirstName("David");
		assertEquals("David", customer.getFirstName());
	}
	
	@Test
	public void testLastNameChange() {
		customer.setLastName("Fox");
		assertEquals("Fox", customer.getLastName());
	}
	
	@Test
	public void testAddressChange() {
		customer.setAddress("Dublin");
		assertEquals("Dublin", customer.getAddress());
	}
	
	@Test
	public void testPhoneNumberChange() {
		customer.setPhoneNumber("0851524451");
		assertEquals("0851524451", customer.getPhoneNumber());
	}
	
	@Test
	public void testEmailChange() {
		customer.setEmail("Jack@hotmail.com");
		assertEquals("Jack@hotmail.com", customer.getEmail());
	}
	
	@Test
	public void testPasswordChange() {
		customer.setPassword("JF1994");
		assertEquals("JF1994", customer.getPassword());
	}
	
	@Test
	public void testAllowingEditing() {
		customer.setCanEdit(true);
		assertTrue(customer.isCanEdit());
	}
	
	@Test
	public void testPurchasedPlanChange() {
		customer.setPurchasedPlan("JF1994");
		assertEquals("JF1994", customer.getPurchasedPlan());
	}
	@Test
	public void testIsMember() {
		customer.setMember(true);
		assertTrue(customer.isMember());
	}
	
	@Test
	public void testToString() {
		assertEquals("Customer [firstName=Jack, lastName=Flynn, address=Galway, phoneNumber=0851558023, email=Jack@gmail.com, password=JF1234, canEdit=false]", customer.toString());
	}
	

}
