package com.ait.customer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

public class CustomerDataTest {
	CustomerData customerData = new CustomerData();

	@Before
	public void setUp() {
		customerData = new CustomerData();
		customerData.init();
	}

	@Test
	public void testGetFirstName() {
		customerData.setFirstName("Jack");
		assertEquals("Jack", customerData.getFirstName());
	}

	@Test
	public void testGetLastName() {
		customerData.setLastName("Flynn");
		assertEquals("Flynn", customerData.getLastName());
	}

	@Test
	public void testGetAddress() {
		customerData.setAddress("Galway");
		assertEquals("Galway", customerData.getAddress());
	}

	@Test
	public void testGetPhoneNumber() {
		customerData.setPhoneNumber("0851558023");
		assertEquals("0851558023", customerData.getPhoneNumber());
	}

	@Test
	public void testGetEmail() {
		customerData.setEmail("Jack@gmail.com");
		assertEquals("Jack@gmail.com", customerData.getEmail());
	}

	@Test
	public void testGetPassword() {
		customerData.setPassword("galway1604");
		assertEquals("galway1604", customerData.getPassword());
	}

	@Test
	public void testInit() {
		assertEquals(3, customerData.getCustomers().size());
		assertEquals("John", customerData.getCustomers().get(0).getFirstName());
		assertEquals("Sheilja", customerData.getCustomers().get(1).getFirstName());
		assertEquals("Rita", customerData.getCustomers().get(2).getFirstName());
	}

	@Test
	public void testObscuredPassword() {
		customerData.setPassword("galway1604");
		assertEquals("g * * * 4", customerData.getObscuredPassword());
	}

	@Test
	public void testEditCustomer() {
		customerData.editCustomer(customerData.getCustomers().get(0));
		assertTrue(customerData.getCustomers().get(0).isCanEdit());
	}

	@Test
	public void testDeleteCustomer() {
		customerData.deleteCustomer(customerData.getCustomers().get(0));
		assertEquals(2, customerData.getCustomers().size());
	}

	@Test
	public void testSaveAction() {
		customerData.editCustomer(customerData.getCustomers().get(0));
		customerData.saveAction();
		assertFalse(customerData.getCustomers().get(0).isCanEdit());
	}

	@Test
	public void testAddCustomer() {
		customerData.setFirstName("Jack");
		customerData.setLastName("Flynn");
		customerData.setAddress("Galway");
		customerData.setPhoneNumber("0851558023");
		customerData.setEmail("Jack@gmail.com");
		customerData.setPassword("galway1604");
		customerData.addCustomer();
		assertEquals(4, customerData.getCustomers().size());
	}

	@Test
	public void testValidRegisteredUser() {
		assertTrue(customerData.validateUser("John@gmail.com", "Johnny"));
	}

	@Test
	public void testInvalidEmailRegisteredUser() {
		assertFalse(customerData.validateUser("John@hotmail.com", "Johnny"));
	}

	@Test
	public void testInvalidPasswordRegisteredUser() {
		assertFalse(customerData.validateUser("John@gmail.com", "Johno"));
	}

	@Test
	public void testValidNewUser() {
		customerData.setFirstName("Jack");
		customerData.setLastName("Flynn");
		customerData.setAddress("Galway");
		customerData.setPhoneNumber("0851558023");
		customerData.setEmail("Jack@gmail.com");
		customerData.setPassword("galway1604");
		customerData.addCustomer();
		assertTrue(customerData.validateUser("Jack@gmail.com", "galway1604"));
	}

	@Test
	public void testInvalidEmailNewUser() {
		customerData.setFirstName("Jack");
		customerData.setLastName("Flynn");
		customerData.setAddress("Galway");
		customerData.setPhoneNumber("0851558023");
		customerData.setEmail("Jack@gmail.com");
		customerData.setPassword("galway1604");
		customerData.addCustomer();
		assertFalse(customerData.validateUser("Jack@hotmail.com", "galway1604"));
	}

	@Test
	public void testInvalidPasswordNewUser() {
		customerData.setFirstName("Jack");
		customerData.setLastName("Flynn");
		customerData.setAddress("Galway");
		customerData.setPhoneNumber("0851558023");
		customerData.setEmail("Jack@gmail.com");
		customerData.setPassword("galway1604");
		customerData.addCustomer();
		assertFalse(customerData.validateUser("Jack@hotmail.com", "galway1605"));
	}

	@Test
	public void testSetPersonalTrainer() {
		customerData.setPersonalTrainer(null);
		assertEquals(null, customerData.getPersonalTrainer());
	}

	@Test
	public void testAddCustomer2() {
		customerData.setFirstName("Jack");
		customerData.setLastName("Flynn");
		customerData.setAddress("Galway");
		customerData.setPhoneNumber("0851558023");
		customerData.setEmail("Jack@watersidegym.com");
		customerData.setPassword("galway1604");
		customerData.addCustomer();

		assertEquals(3, customerData.getCustomers().size());
	}

	@Test
	public void testValidateTrainer() {
		assertEquals(false, customerData.validateTrainer("name", "password"));
	}

	@Test
	public void testValidateTrainer2() {
		assertTrue(customerData.validateTrainer("John@watersidegym.com", "Johnny"));
	}

	@Test
	public void testDuplicateUser() {
		assertEquals(true, customerData.duplicateUser("John@gmail.com"));
	}
	@Test
	public void testDuplicateUser2() {
		customerData.setEmail("John@watersidegym.com");
		assertEquals(false, customerData.duplicateUser("John@watersidegym.com"));
	}

	@Test
	public void testReturnLastName() {
		assertEquals("", customerData.returnLastName("John@gmail.com", "password"));
	}

	@Test
	public void testReturnLastNameExists() {
		assertEquals("Smith", customerData.returnLastName("John@gmail.com", "Johnny"));
	}

	@Test
	public void testReturnFirstName() {
		assertEquals("", customerData.returnFirstName("John@gmail.com", "password"));
	}

	@Test
	public void testReturnFirstNameExists() {
		assertEquals("John", customerData.returnFirstName("John@gmail.com", "Johnny"));
	}

	@Test
	public void testReturnAddress() {
		assertEquals("", customerData.returnAddress("John@gmail.com", "password"));
	}

	@Test
	public void testReturnAddressExists() {
		assertEquals("Athlone", customerData.returnAddress("John@gmail.com", "Johnny"));
	}

	@Test
	public void testReturnPhoneNumber() {
		assertEquals("", customerData.returnPhoneNumber("John@gmail.com", "password"));
	}

	@Test
	public void testReturnPhoneNumberExists() {
		assertEquals("08912345", customerData.returnPhoneNumber("John@gmail.com", "Johnny"));
	}

	@Test
	public void testReturnPassword() {
		assertEquals("", customerData.returnPassword("John@gmail.com", "password"));
	}

	@Test
	public void testReturnPasswordExists() {
		assertEquals("Johnny", customerData.returnPassword("John@gmail.com", "Johnny"));
	}

	@Test
	public void testReturnCustomerName() {
		assertEquals("", customerData.returnCustomerName("Joh22n@gmail.com"));
	}

	@Test
	public void testReturnCustomerExists() {
		assertEquals("John", customerData.returnCustomerName("John@gmail.com"));
	}

	@Test
	public void testReturnMemberPlan() {
		assertEquals("", customerData.returnMemberPlan("Joh22n@gmail.com"));
	}

	@Test
	public void testReturnMemberPlanExists() {
		assertEquals("No plan purchased", customerData.returnMemberPlan("John@gmail.com"));
	}

	@Test
	public void testFindCustomer() {
		ArrayList empty = new ArrayList();
		assertEquals(empty, customerData.findCustomer("Joh22n@gmail.com"));
	}

	@Test
	public void testIsEditMode() {
		customerData.setEditMode(true);
		assertEquals(true, customerData.isEditMode());
	}

	@Test
	public void testGetTotalCustomers() {
		assertEquals(3, customerData.getTotalCustomers());
	}

	@Test
	public void testSwitchToEdit() {
		assertEquals("TrainerDashboard.xhtml", customerData.switchToEdit("John@watersidegym.com"));
	}
	@Test
	public void testSwitchToDashboard() {
		assertEquals("TrainerDashboard.xhtml", customerData.switchToDashboard());
	}
	
	@Test
	public void testUpdateCurrentCustomer() {
		customerData.setEmail("John@watersidegym.com");
		assertEquals("TrainerDashboard.xhtml", customerData.updateCurrentCustomer());
	}
	
	@Test
	public void testGetTotalTrainers() {
		assertEquals(3, customerData.getTotalTrainers());
	}

	@Test
	public void testFindCustomerExists() {

		// assertEquals(customerData.getCustomers().get(0).toString(),customerData.findCustomer("John@gmail.com"));
		ArrayList empty = new ArrayList();
		assertNotEquals(empty, customerData.findCustomer("John@gmail.com"));
		// assertArrayEquals(null, null);
	}
}
