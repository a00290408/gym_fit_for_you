package com.ait.customer;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class MailerTest {

	private Mailer mailer;
	@Before
	public void setUp() {
		mailer = new Mailer();
		mailer.init();
	}
	
	@Test
	public void testInit() {
		assertEquals("ezscrumezgo@gmail.com", mailer.getEmailTo());
		assertEquals("smtp.gmail.com", mailer.getSmtpHostServer());
		assertEquals(null, mailer.getDisplayMessage());
	}


	@Test
	public void testSetEmailFrom() {
		mailer.setEmailFrom("SecondPost");
		assertEquals("SecondPost", mailer.getEmailFrom());
	}

	@Test
	public void testSetEmailTo() {
		mailer.setEmailTo("SecondPost");
		assertEquals("SecondPost", mailer.getEmailTo());
	}

	@Test
	public void testSetMailSubject() {
		mailer.setMailSubject("SecondPost");
		assertEquals("SecondPost", mailer.getMailSubject());
	}

	@Test
	public void testSetMailBody() {
		mailer.setMailBody("SecondPost");
		assertEquals("SecondPost", mailer.getMailBody());
	}

	@Test
	public void testSetDisplayMessage() {
		mailer.setDisplayMessage("SecondPost");
		assertEquals("SecondPost", mailer.getDisplayMessage());
	}

	@Test
	public void testSetSmtpHostServer() {
		mailer.setSmtpHostServer("SecondPost");
		assertEquals("SecondPost", mailer.getSmtpHostServer());
	}


}
