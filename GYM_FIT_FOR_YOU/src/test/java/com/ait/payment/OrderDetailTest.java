package com.ait.payment;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.ait.customer.Post;

public class OrderDetailTest {
private OrderDetail orderDetail;
	
	@Before
	public void setUp() {
		orderDetail = new OrderDetail("product",1.0f,1.0f,1.0f);
	}

	@Test
	public void testGetProductName() {
		assertEquals("product", orderDetail.getProductName());
	}

	@Test
	public void testGetSubtotal() {
		assertEquals("1.00", orderDetail.getSubtotal());
	}

	@Test
	public void testGetTax() {
		assertEquals("1.00", orderDetail.getTax());
	}

	@Test
	public void testGetTotal() {
		assertEquals("1.00", orderDetail.getTotal());
	}

}
