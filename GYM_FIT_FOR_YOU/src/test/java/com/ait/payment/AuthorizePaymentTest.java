package com.ait.payment;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import com.ait.customer.Post;

public class AuthorizePaymentTest {
private AuthorizePayment authorizePayment;
	
	@Before
	public void setUp() {
		authorizePayment = new AuthorizePayment();
	}

	@Test
	public void testSetMembership() {
		authorizePayment.setMembership("events");
		assertEquals("events", authorizePayment.getMembership());
	}

	@Test
	public void testSetDuration() {
		authorizePayment.setDuration(1);
		assertEquals(1, authorizePayment.getDuration());
	}

	@Test
	public void testSetSubTotal() {
		authorizePayment.setSubTotal(50);
		assertEquals(50, authorizePayment.getSubTotal(),0);
	}

	@Test
	public void testSetTax() {
		authorizePayment.setTax(0);
		assertEquals(0, authorizePayment.getTax(),0);
	}

	@Test
	public void testSetTotal() {
		authorizePayment.setTotal(0);
		assertEquals(0, authorizePayment.getTotal(),0);
	}

	@Test
	public void testSetPlan() {
		authorizePayment.setPlan("events");
		assertEquals("events", authorizePayment.getPlan());
	}

	@Test
	public void testGetProduct1() {
		authorizePayment.setPlan("1.2");
		assertEquals("Pro plan - Single membership", authorizePayment.getProduct());
	}
	@Test
	public void testGetProduct2() {
		authorizePayment.setPlan("1.5");
		authorizePayment.setMembership("80");
		assertEquals("Premium plan - Dual membership", authorizePayment.getProduct());
	}
	@Test
	public void testGetProduct3() {
		authorizePayment.setProduct("");
		authorizePayment.setPlan("1.5");
		authorizePayment.setMembership("45");
		assertEquals("Premium plan - Senior membership", authorizePayment.getProduct());
	}
	@Test
	public void testGetProduct4() {
		authorizePayment.setPlan("1");
		authorizePayment.setMembership("300");
		assertEquals("Regular plan - Group membership", authorizePayment.getProduct());
	}
	@Test
	public void testGetProduct5() {
		authorizePayment.setProduct("");
		authorizePayment.setPlan("1.5");
		authorizePayment.setMembership("30");
		assertEquals("Premium plan - Student membership", authorizePayment.getProduct());
	}
	@Test
	public void testRedirectWithPlan() {
		assertEquals("checkout.xhtml", authorizePayment.redirectWithPlan(""));
	}
	

}
