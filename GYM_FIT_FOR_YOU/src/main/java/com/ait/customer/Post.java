package com.ait.customer;

public class Post {

	private String title;
	private String content;
	private String author;
	private String date;
	private boolean canEdit;
	private String media;
	private String className="";
	private boolean image = false;
	private boolean membersOnly;
	
	public Post(String title, String content, String author, String date, String media,String className,boolean membersOnly) {
		super();
		this.title = title;
		this.content = content;
		this.author = author;
		this.date = date;
		this.media = media;
		this.className=className;
	}
	public Post(String title, String content, String author, String date, String media, boolean image, boolean membersOnly) {
		super();
		this.title = title;
		this.content = content;
		this.author = author;
		this.date = date;
		this.media = media;
		this.image = image;
		this.membersOnly=membersOnly;
	}
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public boolean isCanEdit() {
		return canEdit;
	}

	public void setCanEdit(boolean canEdit) {
		this.canEdit = canEdit;
	}

	public String getmedia() {
		return media;
	}

	public void setmedia(String media) {
		this.media = media;
	}
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	
	public boolean isImage() {
		return image;
	}

	public void setImage(boolean image) {
		this.image = image;
	}
	
	@Override
	public String toString() {
		return "Post [title=" + title + ", content=" + content + ", author=" + author + ", date=" + date + ", canEdit="
				+ canEdit + ", media=" + media + ", className=" + className + ", image=" + image + "]";
	}
	public boolean isMembersOnly() {
		return membersOnly;
	}
	public void setMembersOnly(boolean membersOnly) {
		this.membersOnly = membersOnly;
	}

	

}
