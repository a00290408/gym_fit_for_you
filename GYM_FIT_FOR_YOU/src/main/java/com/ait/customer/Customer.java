package com.ait.customer;

public class Customer {
	private String firstName;
	private String lastName;
	private String address;
	private String phoneNumber;
	private String email;
	private String password;
	private boolean canEdit;
	private boolean member;
	private String purchasedPlan = "No plan purchased";
	
	public boolean isMember() {
		return member;
	}
	public void setMember(boolean member) {
		this.member = member;
	}
	public Customer(String firstName, String lastName, String address, String phoneNumber, String email,String password) {
		 this.firstName =firstName;
		 this.lastName = lastName;
		 this.address = address;
		 this.phoneNumber = phoneNumber;
		 this.email = email;
		 this.setPassword(password);
	 } 
	 public boolean isCanEdit() {
			return canEdit;
		}
	 public void setCanEdit(boolean canEdit) {
			this.canEdit = canEdit;
		}
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}
	@Override
	public String toString() {
		return "Customer [firstName=" + firstName + ", lastName=" + lastName + ", address=" + address + ", phoneNumber="
				+ phoneNumber + ", email=" + email + ", password=" + password + ", canEdit=" + canEdit + "]";
	}
	public String getPurchasedPlan() {
		return purchasedPlan;
	}
	public void setPurchasedPlan(String purchasedPlan) {
		this.purchasedPlan = purchasedPlan;
	}

}
