package com.ait.customer;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;


@ManagedBean
@SessionScoped
public class LoginBean {
	String email, password;
	boolean loggedIn=false,loggedInAsPT=false;
	static String email2="";
	static String firstName="";
	static String lastName="";
	static String address="";
	static String phoneNumber="";
	static String email3="";
	static String password2="";
	boolean member = false;
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
		email2=email;
		if(email2!="") {
			loggedIn=true;
		}else {
			loggedIn=false;
		}
		if(email2.contains("@watersidegym.com")) {
			member=true;
			loggedInAsPT=true;
		}else {
			loggedInAsPT=false;
		
		}
		System.out.print(loggedInAsPT);
		//System.out.print(loggedin);
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String validateCustomerLogin() {
		CustomerData customerData = Helper.getBean("customerData", CustomerData.class);

		if (email.contains("@watersidegym")) {
			if (customerData.validateTrainer(email, password)) {
				firstName=customerData.returnFirstName(email, password);
				lastName=customerData.returnLastName(email, password);
				address=customerData.returnAddress(email, password);
				phoneNumber=customerData.returnPhoneNumber(email, password);
				password=customerData.returnPassword(email, password);
				loggedInAsPT=true;
				return "TrainerDashboard.xhtml";
			}
			else {
				showErrorMessage();
				return (null);
			}
		}	
		else if (customerData.validateUser(email, password)) {
			loggedIn=true;
			return "CustomerDashboard.xhtml";
		} else {
			showErrorMessage();
			return (null);
		}
	}
	
	public static void showErrorMessage() {
		FacesContext context=FacesContext.getCurrentInstance();
		FacesMessage errorMessage=
				new FacesMessage ("Invalid Username/Password Combination");
		errorMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
		context.addMessage(null,  errorMessage);
	}
	
	public String logoutUser() {
		System.out.print("in logout");
		this.email="";
		this.loggedIn=false;
		this.loggedInAsPT=false;
		this.password="";
		this.member=false;
		email2="";
		email3="";
		password2="";
		phoneNumber="";
		address="";
		firstName="";
		lastName="";
		/*HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		if(session!=null) {
			session.invalidate();
		}*/
		return "index.xhtml";
	}

	public boolean isLoggedIn() {
		return loggedIn;
	}

	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;
	}

	public boolean isLoggedInAsPT() {
		return loggedInAsPT;
	}

	public void setLoggedInAsPT(boolean loggedInAsPT) {
		this.loggedInAsPT = loggedInAsPT;
	}

	public static String getEmail2() {
		return email2;
	}

	public static void setEmail2(String email2) {
		LoginBean.email2 = email2;
	}
	
	public String redirectAndSetMembership(String purchasedPlan) {
		CustomerData customerData = Helper.getBean("customerData", CustomerData.class);
		for (Customer customer: customerData.getCustomers()) {
			if (customer.getEmail().equals(this.email) && customer.getPassword().equals(this.password)) {
				this.member=true;
				customer.setMember(true);
				customer.setPurchasedPlan(purchasedPlan);
				return "CustomerDashboard.xhtml";
			}
		}
		return "Login.xhtml";
	}

	public boolean isMember() {
		return member;
	}

	public void setMember(boolean member) {
		this.member = member;
	}
}