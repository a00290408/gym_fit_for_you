package com.ait.customer;

import java.util.ArrayList;

public class Classes {

	public Classes(String className, String information, String trainer, String dateTime, int capacity, String participant,boolean individualProgram, String individualCustomer) {
		super();
		this.className = className;
		this.information = information;
		this.trainer = trainer;
		this.dateTime = dateTime;
		this.participant=participant;
		this.capacity=capacity;
		this.individualProgram = individualProgram;
		if(capacity>0) {
			this.spacesAvailable=true;
		}else {
			this.spacesAvailable=false;
		}
		this.alreadyBooked=false;
		this.timeConflict=false;
		setCustomerList();
		this.individualCustomer=individualCustomer;
	}

	private String className;
	private String information;
	private String trainer;
	private String dateTime;
	private boolean canEdit;
	private boolean canAdd;
	private int capacity;
	private boolean spacesAvailable;
	private boolean alreadyBooked;
	private boolean timeConflict;
	private boolean individualProgram;
	private String participant;
	private ArrayList<Customer> customerList;
	private String individualCustomer;
	
	public String getIndividualCustomer() {
		return individualCustomer;
	}

	public void setIndividualCustomer(String individualCustomer) {
		this.individualCustomer = individualCustomer;
	}
	
	public ArrayList<Customer> getCustomerList() {
		return customerList;
	}

	public void setCustomerList() {
		customerList=new ArrayList<Customer>();
	}
	
	public void addCustomer(Customer customer) {
		customerList.add(customer);
	}
	
	public void removeCustomer(Customer customer) {
		customerList.remove(customer);
	}
	
	public String getParticipant() {
		return participant;
	}

	public void setParticipant(String participant) {
		this.participant = participant;
	}
	public void addParticipant() {
		this.participant += LoginBean.email2+", ";
	}
	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getInformation() {
		return information;
	}

	public void setInformation(String information) {
		this.information = information;
	}

	public String getTrainer() {
		return trainer;
	}

	public void setTrainer(String trainer) {
		this.trainer = trainer;
	}

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	public boolean isCanEdit() {
		return canEdit;
	}

	public void setCanEdit(boolean canEdit) {
		this.canEdit = canEdit;
	}

	public boolean isCanAdd() {
		return canAdd;
	}

	public void setCanAdd(boolean canAdd) {
		this.canAdd = canAdd;
	}
	
	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}
	
	public void removePlace() {
		this.capacity--;
		if(this.capacity<1) {
			this.spacesAvailable=false;
		}
	}
	
	public boolean isSpacesAvailable() {
		return spacesAvailable;
	}

	public void setSpacesAvailable(boolean spacesAvailable) {
		this.spacesAvailable = spacesAvailable;
	}
	
	public boolean isAlreadyBooked() {
		return alreadyBooked;
	}

	public void setAlreadyBooked(boolean alreadyBooked) {
		this.alreadyBooked = alreadyBooked;
	}
	
	public void participantAdded() {
		this.alreadyBooked=true;
	}
	
	public void doNothing() {
		
	}

	public boolean isTimeConflict() {
		return timeConflict;
	}

	public void setTimeConflict(boolean timeConflict) {
		this.timeConflict = timeConflict;
	}

	@Override
	public String toString() {
		return "Classes [className=" + className + ", information=" + information + ", trainer=" + trainer
				+ ", dateTime=" + dateTime + ", canEdit=" + canEdit + ", canAdd=" + canAdd + ", capacity=" + capacity
				+ ", spacesAvailable=" + spacesAvailable + ", alreadyBooked=" + alreadyBooked + ", timeConflict="
				+ timeConflict + ", participant=" + participant + "]";
	}

	public boolean isIndividualProgram() {
		return individualProgram;
	}

	public void setIndividualProgram(boolean individualProgram) {
		this.individualProgram = individualProgram;
	}
}
