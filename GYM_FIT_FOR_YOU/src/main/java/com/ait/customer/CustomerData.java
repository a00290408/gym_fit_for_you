package com.ait.customer;

import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

@ManagedBean
@ApplicationScoped
public class CustomerData {
	private ArrayList<Customer> personalTrainer;
	private ArrayList<Customer> customers;
	private String firstName;
	private String lastName;
	private String address;
	private String phoneNumber;
	private String email;
	private String password;
	private ArrayList<Customer> customerDashboard;

	@PostConstruct
	public void init() {

		customers = new ArrayList<Customer>();
		Customer customerOne = new Customer("John", "Smith", "Athlone", "08912345", "John@gmail.com", "Johnny");
		customers.add(customerOne);
		Customer customerTwo = new Customer("Sheilja", "Bansal", "Moate", "08765432", "Sheilj123@gmail.com", " ");
		customers.add(customerTwo);
		Customer customerThree = new Customer("Rita", "Styles", "Roscommon", "08765213", "rita@gmail.com", "Riitaa");
		customers.add(customerThree);
		personalTrainer = new ArrayList<Customer>();
		Customer personalTrainer1 = new Customer("John", "Smith", "Athlone", "08912345", "John@watersidegym.com", "Johnny");
		personalTrainer.add(personalTrainer1);
		Customer personalTrainer2 = new Customer("Sheilja", "Bansal", "Moate", "08765432", "Sheilj@watersidegym.com", "Sheilj");
		personalTrainer.add(personalTrainer2);
		Customer personalTrainer3 = new Customer("Rita", "Styles", "Roscommon", "08765213", "rita@watersidegym.com", " ");
		personalTrainer.add(personalTrainer3);

		customerDashboard=new ArrayList<Customer>();
	}
	public ArrayList<Customer> getPersonalTrainer() {
		return personalTrainer;
	}

	public void setPersonalTrainer(ArrayList<Customer> personalTrainer) {
		this.personalTrainer = personalTrainer;
	}

	public ArrayList<Customer> getCustomers() {
		return customers;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getObscuredPassword() {
		return (firstLetter(password) + " * * * " + lastLetter(password));
	}

	private String firstLetter(String s) {
		return (s.substring(0, 1));
	}

	private String lastLetter(String s) {
		int length = s.length();
		return (s.substring(length - 1, length));
	}

	public String editCustomer(Customer customer) {
		customer.setCanEdit(true);
		return null;
	}

	public void deleteCustomer(Customer customer) {
		customers.remove(customer);
	}

	public String saveAction() {
		for (Customer customer : customers) {
			customer.setCanEdit(false);

		}
		return null;
	}

	public String addCustomer() {
		final Customer customer = new Customer(this.firstName, this.lastName, this.address, this.phoneNumber,
				this.email, this.password);
		FacesContext context=FacesContext.getCurrentInstance();
		if (email.contains("@watersidegym")) {
			personalTrainer.add(customer);
		} 
		else if(duplicateUser(customer.getEmail())) {
			FacesMessage errorMessage= new FacesMessage ("Email already in use, Use Unique Email Address");
			errorMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage("regform:regbutton", errorMessage);
			return "";
		} 
		else {
			customers.add(customer);
		}
		return ("show-registration");

	}

	public boolean validateUser(String email, String password) {
		boolean validUser = false;
		for (Customer customer : customers) {
			if ((customer.getEmail().equals(email)) && (customer.getPassword().equals(password))) {
				validUser = true;
				break;
			}
		}
		return validUser;
	}

	public boolean validateTrainer(String email, String password) {
		boolean validUser = false;
		for (Customer trainer : personalTrainer) {
			if ((trainer.getEmail().equals(email)) && (trainer.getPassword().equals(password))) {
				validUser = true;
				break;
			}
		}
		return validUser;
	}

	public boolean duplicateUser(String email) {
		boolean user=false;
		for (Customer customer:customers) {
			if((customer.getEmail().equals(email))){
				user=true;
				break;
			}
		}
		return user;
	}

	public String returnFirstName(String email, String password) {
		String firstName="";
		for (Customer customer : customers) {
			if ((customer.getEmail().equals(email)) && (customer.getPassword().equals(password))) {
				firstName=customer.getFirstName();
				break;
			}
		}
		return firstName;
	}

	public String returnLastName(String email, String password) {
		String lastName="";
		for (Customer customer : customers) {
			if ((customer.getEmail().equals(email)) && (customer.getPassword().equals(password))) {
				lastName=customer.getLastName();
				break;
			}
		}
		return lastName;
	}

	public String returnAddress(String email, String password) {
		String address="";
		for (Customer customer : customers) {
			if ((customer.getEmail().equals(email)) && (customer.getPassword().equals(password))) {
				address=customer.getAddress();
				break;
			}
		}
		return address;
	}

	public String returnPhoneNumber(String email, String password) {
		String phoneNumber="";
		for (Customer customer : customers) {
			if ((customer.getEmail().equals(email)) && (customer.getPassword().equals(password))) {
				phoneNumber=customer.getPhoneNumber();
				break;
			}
		}
		return phoneNumber;
	}

	public String returnPassword(String email, String password) {
		String password1="";
		for (Customer customer : customers) {
			if ((customer.getEmail().equals(email)) && (customer.getPassword().equals(password))) {
				password1=customer.getPassword();
				break;
			}
		}
		return password1;
	}
	
	
	

	// For Trainer Dashboard

	private boolean editMode = false;

	public boolean isEditMode() {
		return editMode;
	}
	public void setEditMode(boolean editMode) {
		this.editMode = editMode;
	}
	
	public int getTotalCustomers() {
		return customers.size();
	}
	
	public int getTotalTrainers() {
		return personalTrainer.size();
	}
	
	public String switchToEdit(String loggedInEmail) {
		for(Customer pt: personalTrainer) {
			if(pt.getEmail().equals(loggedInEmail)) {
				this.setAddress(pt.getAddress());
				this.setEmail(pt.getEmail());
				this.setFirstName(pt.getFirstName());
				this.setLastName(pt.getLastName());
				this.setPassword(pt.getPassword());
				this.setPhoneNumber(pt.getPhoneNumber());
			}
		}
		
		setEditMode(true);
		return "TrainerDashboard.xhtml";
	}
	public String switchToDashboard() {
		System.out.println("called");
		setEditMode(false);
		return "TrainerDashboard.xhtml";
	}
	public String updateCurrentCustomer() {
		for(Customer pt: personalTrainer) {
			if(pt.getEmail().equals(this.getEmail())) {
				pt.setAddress(this.getAddress());
				pt.setEmail(this.getEmail());
				pt.setFirstName(this.getFirstName());
				pt.setLastName(this.getLastName());
				pt.setPassword(this.getLastName());
				pt.setPhoneNumber(this.getPhoneNumber());
			}
		}
		setEditMode(false);
		return "TrainerDashboard.xhtml";
	}
	
	public ArrayList<Customer> findCustomer(String participant) {
		System.out.println(participant);
		customerDashboard.clear();
		for (Customer myCustomer : customers) {
			if (myCustomer.getEmail().contains(participant)) {
				customerDashboard.add(myCustomer);
				return customerDashboard;
			}
		}
		return customerDashboard;
	}
	
	public String returnMemberPlan(String participant) {
		String plan="";
		for (Customer myCustomer : customers) {
			if (myCustomer.getEmail().contains(participant)) {
				plan=myCustomer.getPurchasedPlan();
				break;
			}
		}
		return plan;
	}
	
	public String returnCustomerName(String participant) {
		String firstName="";
		for (Customer customer : customers) {
			if (customer.getEmail().equals(participant)) {
				firstName=customer.getFirstName();
				break;
			}
		}
		return firstName;
	}
}