package com.ait.customer;

import java.util.ArrayList;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean
@SessionScoped
public class ClassesData {

	private ArrayList<Classes> myClasses;
	private String className = "";
	private String information;
	private String participant;
	private String trainer;
	private String dateTime;
	private int capacity;
	private boolean individualProgram;
	boolean canAdd = true;
	private ArrayList<Classes> customerClasses;
	private ArrayList<Classes> customerDashboard;
	public String individualCustomer;

	@PostConstruct
	public void init() {

		myClasses = new ArrayList<Classes>();
		Classes classesOne = new Classes("Aerobics", "This class is ideal for those looking to get into aerobics",
				"Jack Flynn", "Tue Aug 24 09:00:00 BST 2021", 20, "", false, "");
		myClasses.add(classesOne);
		Classes classesTwo = new Classes("Strongman", "Competing to become a strongman? this class is ideal for you!",
				"Cormac Rafftery", "Tue Aug 24 09:00:00 BST 2021", 15, "",false, "");
		myClasses.add(classesTwo);
		Classes classesThree = new Classes("Cardio", "Tired of being constantly out of breath? Running training here!",
				"David Daly", "Tue Aug 24 12:00:00 BST 2021", 0, "",false, "");
		myClasses.add(classesThree);
		Classes classesFour = new Classes("Boxing", "This class is ideal for those looking to get into aerobics",
				"Peter Lee", "Tue Aug 24 16:00:00 BST 2021", 20, "",false, "");
		myClasses.add(classesFour);
		Classes classesFive = new Classes("Core", "Competing to become a strongman? this class is ideal for you!",
				"Cormac Rafftery", "Tue Aug 24 18:00:00 BST 2021", 15, "",false, "");
		myClasses.add(classesFive);
		Classes classesSix = new Classes("Zumba", "Tired of being constantly out of breath? Running training here!",
				"Carl Jones", "Tue Aug 24 20:00:00 BST 2021", 1, "",false, "");
		myClasses.add(classesSix);
		customerClasses=new ArrayList<Classes>();
		customerDashboard=new ArrayList<Classes>();
	}

	public boolean isPT() {
		if(LoginBean.email2.contains("@watersidegym.com")) {
			return true;
		}
		else {
			return false;
		}
	}

	public void addCustomer(Classes classes) {
		Customer customer=new Customer(LoginBean.firstName, LoginBean.lastName, LoginBean.address, LoginBean.email2, LoginBean.email2, LoginBean.email2);
		classes.addCustomer(customer);
		classes.removePlace();
		classes.participantAdded();
		classes.setCanEdit(false);
		customerClasses.add(classes);
		// Determining time conflicts after booking class
		for(Classes eachClass:myClasses) {
			if(eachClass.getDateTime().equals(classes.getDateTime())) {
				eachClass.setTimeConflict(true);
			}
		}
	}

	public String addParticipant(Classes classes) {
		classes.setCanEdit(true);
		classes.addParticipant();
		classes.removePlace();
		classes.participantAdded();
		classes.setCanEdit(false);
		customerClasses.add(classes);
		// Determining time conflicts after booking class
		for(Classes eachClass:myClasses) {
			if(eachClass.getDateTime().equals(classes.getDateTime())) {
				eachClass.setTimeConflict(true);
			}
		}
		return null;
	}
	
	public void removeClass(Classes classes) {
		for(Customer customer:classes.getCustomerList()) {
			if(LoginBean.email2.equals(customer.getEmail())) {
				classes.removeCustomer(customer);
				customerClasses.remove(classes);
				myClasses.get(myClasses.indexOf(classes)).setCapacity(myClasses.get(myClasses.indexOf(classes)).getCapacity()+1);
				break;
			}
		}
		
		myClasses.get(myClasses.indexOf(classes)).setSpacesAvailable(true);
		myClasses.get(myClasses.indexOf(classes)).setAlreadyBooked(false);
		myClasses.get(myClasses.indexOf(classes)).setTimeConflict(false);
		for(Classes eachClass:myClasses) {
			if(eachClass.getDateTime().equals(classes.getDateTime())) {
				eachClass.setTimeConflict(false);
			}
		}
	}
	
	public void removeIndividualProgram(String name) {
		for(Classes eachClass:myClasses) {
			if(eachClass.getIndividualCustomer().equals(name)) {
				myClasses.remove(eachClass);
				customerClasses.remove(eachClass);
				break;
			}
		}
	}
	
	public String getIndividualCustomer() {
		return individualCustomer;
	}

	public void setIndividualCustomer(String individualCustomer) {
		this.individualCustomer = individualCustomer;
	}
	
	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public ArrayList<Classes> getMyClasses() {
		return myClasses;
	}

	public void setMyClasses(ArrayList<Classes> myClasses) {
		this.myClasses = myClasses;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getInformation() {
		return information;
	}

	public void setInformation(String information) {
		this.information = information;
	}

	public String getTrainer() {
		return trainer;
	}

	public void setTrainer(String trainer) {
		this.trainer = trainer;
	}

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	public String editClasses(Classes classes) {
		classes.setCanEdit(true);
		return null;
	}

	public String deleteClasses(Classes classes) {
		myClasses.remove(classes);
		return null;
	}

	public String saveAction(Classes classes) {
		classes.setCanEdit(false);
		System.out.println(classes.getClassName());
		System.out.println(classes.getParticipant());
		return null;
	}

	public String addClasses() {
		String beginningTime=this.dateTime.substring(0, 11);
		String stringHours=this.dateTime.substring(11, 13);
		String remainingTime=this.dateTime.substring(13);
		int hours = Integer.parseInt(stringHours);
		if(hours==00) {
			hours=23;
			this.dateTime=beginningTime+hours+remainingTime;
		} else {
			hours--;
			if(hours<10) {
				this.dateTime=beginningTime+0+hours+remainingTime;
			} else {
				this.dateTime=beginningTime+hours+remainingTime;
			}
		}
		final Classes classes = new Classes(this.className, this.information, this.trainer, this.dateTime, this.capacity,
				this.participant,this.individualProgram, this.individualCustomer);
		myClasses.add(classes);
		if(classes.isIndividualProgram()==true) {
			customerClasses.add(classes);
		}
		this.className=null;
		this.information=null;
		this.trainer=null;
		this.dateTime=null;
		this.capacity=0;
		this.individualProgram=false;
		this.individualCustomer="";
		return ("Classes");
	}

	public void setCanAdd(boolean addBool) {
		canAdd = addBool;
	}

	public String getParticipant() {
		return participant;
	}

	public void setParticipant(String participant) {
		System.out.println("in setParticipant in classesData");
		this.participant=participant;
	}

	public boolean isCanAdd() {
		return canAdd;
	}

	public ArrayList<Classes> getCustomerClasses() {
		return customerClasses;
	}

	public void setCustomerClasses(ArrayList<Classes> customerClasses) {
		this.customerClasses = customerClasses;
	}

	public boolean isIndividualProgram() {
		return individualProgram;
	}

	public void setIndividualProgram(boolean individualProgram) {
		this.individualProgram = individualProgram;
	}
	
	// For Trainer Dashboard
	public int getTotalClasses() {
		return myClasses.size();
	}
	public int getTotalRegisteredCustomers() {
		return customerClasses.size();
	}
	
	public ArrayList<Classes> findCustomerClasses(String participant) {
		System.out.println(participant);
		customerDashboard.clear();
		for (Classes myClass : myClasses) {
			for (Customer customer : myClass.getCustomerList()) {
				if (customer.getEmail().contains(participant)) {
					customerDashboard.add(myClass);
				}
			}
		}
		return customerDashboard;
	}
	
    private Date dateToday = new Date();
	
	public Date getDateToday() {
		return dateToday;
	}

}