package com.ait.customer;

import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import javax.mail.Session;

@ManagedBean
@RequestScoped
public class Mailer {
	private String emailFrom;
	private String emailTo;
	private String mailSubject;
	private String mailBody;
	private String smtpHostServer;
	private String displayMessage;
	
	@PostConstruct
	public void init() {
		this.setEmailTo("ezscrumezgo@gmail.com");
		this.setSmtpHostServer("smtp.gmail.com");
		this.setDisplayMessage(null);
//		System.out.println("SimpleEmail Start");
	}
		    

	public String getEmailFrom() {
		return emailFrom;
	}

	public void setEmailFrom(String emailFrom) {
		this.emailFrom = emailFrom;
	}

	public String getEmailTo() {
		return emailTo;
	}

	public void setEmailTo(String emailTo) {
		this.emailTo = emailTo;
	}

	public String getMailSubject() {
		return mailSubject;
	}

	public void setMailSubject(String mailSubject) {
		this.mailSubject = mailSubject;
	}

	public String getMailBody() {
		return mailBody;
	}

	public void setMailBody(String mailBody) {
		this.mailBody = mailBody;
	}

	public String getDisplayMessage() {
		return displayMessage;
	}

	public void setDisplayMessage(String displayMessage) {
		this.displayMessage = displayMessage;
	}
	
	public String getSmtpHostServer() {
		return smtpHostServer;
	}

	public void setSmtpHostServer(String smtpHostServer) {
		this.smtpHostServer = smtpHostServer;
	}
	
	public String sendEmail() {
	    
	    Properties props = System.getProperties();

	    props.put("mail.smtp.host", this.smtpHostServer);
	    props.put("mail.smtp.starttls.enable", "true");

	    props.put("mail.smtp.port", "587");

	    Session session = Session.getInstance(props, null);
	    
	    EmailUtil.sendEmail(session, this.emailTo, this.emailFrom, this.mailSubject, this.mailBody);
	    
	    this.setEmailFrom(null);
	    this.setMailSubject(null);
	    this.setMailBody(null);
	    this.setDisplayMessage("Email Sent Successfully!!");
	    
	    return("Contact");
		
	}
}
