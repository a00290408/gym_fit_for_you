package com.ait.customer;

import java.util.Date;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class EmailUtil {
	public static void sendEmail(Session session, String emailTo, String emailFrom, String subject, String body){
		try
	    {
		    
	      MimeMessage msg = new MimeMessage(session);
	      //set message headers
	      msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
	      msg.addHeader("format", "flowed");
	      msg.addHeader("Content-Transfer-Encoding", "8bit");
	
	      msg.setFrom(new InternetAddress(emailFrom, "GYM FIT FOR YOU"));
	
	      msg.setReplyTo(InternetAddress.parse(emailFrom, false));
	
	      msg.setSubject(subject, "UTF-8");
	
	      msg.setText(body, "UTF-8");
	
	      msg.setSentDate(new Date());
	
	      msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailTo, false));
//	      System.out.println("Message is ready");
	      
	      Transport transport = session.getTransport("smtp");

	      transport.connect("ezscrumezgo@gmail.com", "Cse@2021");
	      transport.sendMessage(msg, msg.getAllRecipients());
	      transport.close();
	      
//		  Transport.send(msg);  
	
//	      System.out.println("Email Sent Successfully!!");
	    }
	    catch (Exception e) {
	      e.printStackTrace();
	    }
	}
}
