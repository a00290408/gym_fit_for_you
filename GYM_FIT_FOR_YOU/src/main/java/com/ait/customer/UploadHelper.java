package com.ait.customer;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.Part;

// Object to hold media properties
class Media {
	private String fileName;
	private boolean image;
	
	public Media(String fileName, boolean image) {
		this.fileName = fileName;
		this.image = image;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public boolean isImage() {
		return image;
	}

	public void setImage(boolean image) {
		this.image = image;
	}

}

public class UploadHelper {

	private final float limit_max_size = 10240000000f;
//	private final int limit_max_size = 10240000;
	private final String limit_type_file = "gif|jpg|png|jpeg|mp4";
//	private final String path_to =  File.separator + "uploaded-images";
//	private final String path_to_images = "resources" + File.separator + "images";
//	private final String path_to_videos = "resources" + File.separator + "videos";
	private final String path_to_media = "resources" + File.separator + "media";
//	private final String path_to = "resources" + File.separator + "images";

	Media uploadMedia = new Media(null, false);
	
	public Media processUpload(Part fileUpload) {
		String fileSaveData = "nomedia.jpg";
		boolean imageFile = true;
//		System.out.println(fileUpload.getName());
		try {
			if (fileUpload.getSize() > 0) {
				String submittedFileName = getFilename(fileUpload);
				if (checkFileType(submittedFileName)) {
					if (fileUpload.getSize() > this.limit_max_size) {
						FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "File size too large!", ""));
						System.out.println("File is too large");
					} else {
						String currentFileName = submittedFileName;
						String extension = currentFileName.substring(currentFileName.lastIndexOf("."), currentFileName.length());
						Long nameRadom = Calendar.getInstance().getTimeInMillis();
//						String newfilename = currentFileName;
						String newfilename = nameRadom + extension;
						fileSaveData = newfilename;
//						System.out.println(extension);
//						System.out.println(fileSaveData);
//						String fileSavePath = FacesContext.getCurrentInstance().getExternalContext().getApplicationContextPath() + this.path_to;
						String fileSavePath = "";
						if(extension.equals(".jpg") || extension.equals(".jpeg") || extension.equals(".png")) {
//							fileSavePath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("") + this.path_to_images;
							imageFile = true;
							fileSavePath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("") + this.path_to_media;
						} else if(extension.equals(".mp4")) {
//							fileSavePath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("") + this.path_to_videos;
							imageFile = false;
							fileSavePath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("") + this.path_to_media;
						}
						else {
							fileSaveData = "nomedia.jpg";
						}
//						System.out.println(fileSavePath);
						try {
							byte[] fileContent = new byte[(int) fileUpload.getSize()];
							InputStream in = fileUpload.getInputStream();
							in.read(fileContent);

							File fileToCreate = new File(fileSavePath, newfilename);
							File folder = new File(fileSavePath);
							if (!folder.exists()) {
								folder.mkdirs();
							}
							FileOutputStream fileOutStream = new FileOutputStream(fileToCreate);
							fileOutStream.write(fileContent);
							fileOutStream.flush();
							fileOutStream.close();
							fileSaveData = newfilename;
						} catch (IOException e) {
							System.out.println("Error handling file");
							fileSaveData = "nomedia.jpg";
						}
					}
				} else {
					System.out.println("File type not valid.");
					fileSaveData = "nomedia.jpg";
				}
			}
		} catch (Exception ex) {
			System.out.println("Unable to upload.");
			fileSaveData = "nomedia.jpg";
		}
		uploadMedia.setFileName(fileSaveData);
		uploadMedia.setImage(imageFile);
		return uploadMedia;
	}

	public String getFilename(Part part) {
		try{for (String cd : part.getHeader("content-disposition").split(";")) {
			if (cd.trim().startsWith("filename")) {
				String filename = cd.substring(cd.indexOf('=') + 1).trim().replace("\"", "");
				return filename.substring(filename.lastIndexOf('/') + 1).substring(filename.lastIndexOf('\\') + 1);
			}
		}
		}catch (Exception ex) {
			return null;
		}
		return null;
	}

	public boolean checkFileType(String fileName) {
		if (fileName.length() > 0) {
			String[] parts = fileName.split("\\.");
			if (parts.length > 0) {
				String extention = parts[parts.length - 1];
				return this.limit_type_file.contains(extention);
			}
		}
		return false;
	}

}
