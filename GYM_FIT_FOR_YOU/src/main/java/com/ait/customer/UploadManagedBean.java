package com.ait.customer;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.servlet.http.Part;

@SessionScoped
@ManagedBean(name = "uploadManagedBean")
public class UploadManagedBean {

	private String media = "";
	private boolean image = false;
	private Part part;


	public String getMedia() {
		return media;
	}

	public void setMedia(String media) {
		this.media = media;
	}

	public boolean isImage() {
		return image;
	}

	public void setImage(boolean image) {
		this.image = image;
	}
	
	public Part getPart() {
		return part;
	}

	public void setPart(Part part) {
		this.part = part;
	}

	public String processUpload() {
		UploadHelper uploadHelper = new UploadHelper();
		this.media = uploadHelper.processUpload(this.part).getFileName();
		this.image = uploadHelper.processUpload(this.part).isImage();
		return "result";
	}



}