
package com.ait.customer;

import java.time.LocalDateTime;
import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.servlet.http.Part;

@ManagedBean
@SessionScoped
public class PostData {

	private ArrayList<Post> posts;
	private String title;
	private String content;
	private String author;
	private String date;
	private String imageName;
	private String media = "";
	private boolean image = false;
	private Part part;
	private boolean membersOnly;

	@PostConstruct
	public void init() {

		posts = new ArrayList<Post>();
		Post postOne = new Post("Portable Home Kits", 
				"If you're searching for a compact set of home exercise equipment that still lets you get full-body workouts, "
				+ "the Waterside Gym ihas all you need. Visit us today.", 
				"cormac1", "30/07/2021 02:25", "pexels-posts-1.jpg", true, false);
		posts.add(postOne);
		Post postTwo = new Post("Gyming During Covid", 
				"If you live in a densely populated area, it can be difficult to keep the recommended distance "
				+ "and the government have stated that unnecessary travel should not be undertaken. "
				+ "If in doubt, take advantage of the multiple online resources and exercise at home with our online classes.", 
				"cormac2", "01/03/2021 14:16", "pexels-posts-2.jpg", true, true);
		posts.add(postTwo);
		Post postThree = new Post("Why You Need a Personal Trainer", 
				"Being educated while exercising is essential in maximizing effectiveness and reducing risk of injury. "
				+ "A personal trainer will teach you everything you need to know about exercising. "
				+ "They will put together the perfect routine to help you achieve your goals, "
				+ "demonstrate the correct posture for each exercise.", 
				"cormac3", "20/02/2019 17:12", "pexels-posts-3.jpg", true, false);
		posts.add(postThree);
	}

	public ArrayList<Post> getPosts() {
		return posts;
	}

	public void setPosts(ArrayList<Post> posts) {
		this.posts = posts;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		
		this.author = LoginBean.email2;

		//this.author = author;
	}
	
	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
	
	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		LocalDateTime now = LocalDateTime.now();
		
		int min=now.getMinute();
		int hour=now.getHour();
		int day=now.getDayOfMonth();
		int month=now.getMonthValue();
		int year=now.getYear();
		this.date=day+"/"+month+"/"+year+" "+String.format("%02d", hour)+":"+String.format("%02d", min);
		//this.date=now.toString();
	}
	public String editPost(Post post) {
		post.setCanEdit(true);
		return null;
	}
	public String deletePost(Post post) {
		posts.remove(post);
		return null;
	}
	public String saveAction() {
		for(Post post:posts) {
			post.setCanEdit(false);
			
		}
		return null;
	}
	
	public String addPost() {
		processUpload();
		final Post post = new Post(this.title,this.content,this.author,this.date, this.media, this.image, this.membersOnly);
		posts.add(post);
		return ("NewsFeed");
	}

	public String getMedia() {
		return media;
	}

	public void setMedia(String media) {
		this.media = media;
	}

	public boolean isImage() {
		return image;
	}

	public void setImage(boolean image) {
		this.image = image;
	}
	
	public Part getPart() {
		return part;
	}

	public void setPart(Part part) {
		this.part = part;
	}
	
	public void processUpload() {
		UploadHelper uploadHelper = new UploadHelper();
		this.media = uploadHelper.processUpload(this.part).getFileName();
		this.image = uploadHelper.processUpload(this.part).isImage();
	}
	public boolean isMembersOnly() {
		return membersOnly;
	}

	public void setMembersOnly(boolean membersOnly) {
		this.membersOnly = membersOnly;
	}


}
