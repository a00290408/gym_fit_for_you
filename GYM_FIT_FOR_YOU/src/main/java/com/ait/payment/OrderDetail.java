package com.ait.payment;
import javax.faces.bean.ManagedBean;
@ManagedBean
public class OrderDetail {
	private String productName;
    private float subtotal;
    private float tax;
    private float total;
 
	public OrderDetail(String productName, float subtotal, float tax, float total) {
		this.productName = productName;
		this.subtotal = subtotal;
        this.tax = tax;
        this.total = total;
    }
 
    public String getProductName() {
        return productName;
    }
 
    public String getSubtotal() {
        return String.format("%.2f", subtotal);
    }
 
    public String getTax() {
        return String.format("%.2f", tax);
    }
     
    public String getTotal() {
        return String.format("%.2f", total);
    }

}
