package com.ait.payment;

import java.io.IOException;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

//import com.paypal.base.rest.PayPalRESTException;
import com.paypal.base.rest.PayPalRESTException;

@ManagedBean
@SessionScoped
public class AuthorizePayment {
	private String product;
	private String plan="1";
	private String membership="50";
	private int duration=1;
	private float subTotal,  tax, total;

	public String getProduct() {
		product = "";
		if (plan.equals("1.2")) {
			product+="Pro";
		} else if (plan.equals("1.5")) {
			product+="Premium";
		} else if (plan.equals("1")){
			product+="Regular";
		}
		product+=" plan - ";
		if (membership.equals("50")) {
			product+="Single";
		} else if (membership.equals("80")) {
			product+="Dual";
		} else if (membership.equals("45")) {
			product+="Senior";
		} else if (membership.equals("30")) {
			product+="Student";
		} else if (membership.equals("300")) {
			product+="Group";
		}
		product+=" membership";
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getMembership() {
		return membership;
	}

	public void setMembership(String membership) {
		this.membership = membership;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}
	
	public float getSubTotal() {
//		float productIdMultiplier = 1.0f;
//		if (product.equals("Pro")) {
//			productIdMultiplier = 1.2f;
//		} else if (product.equals("Premium")) {
//			productIdMultiplier = 1.5f;
//		}
//		float membershipAmount = 0;
//		if (membership.equals("single")) {
//			membershipAmount = 50;
//		} else if (membership.equals("dual")) {
//			membershipAmount = 80;
//		} else if (membership.equals("senior")) {
//			membershipAmount = 45;
//		} else if (membership.equals("student")) {
//			membershipAmount = 30;
//		} else if (membership.equals("group")) {
//			membershipAmount = 300;
//		}
		subTotal = Float.valueOf(plan)*Float.valueOf(membership)*duration;
		return subTotal;
	}

	public void setSubTotal(float subTotal) {
		this.subTotal = subTotal;
	}

	

	public float getTax() {
		tax = subTotal*1/100;
		return tax;
	}

	public void setTax(float tax) {
		this.tax = tax;
	}

	public float getTotal() {
		total = subTotal + tax;
		return total;
	}

	public void setTotal(float total) {
		this.total = total;
	}

	public String checkOut() throws IOException {
		try {
			OrderDetail orderDetail = new OrderDetail(product, subTotal, tax, total);
			PaymentServices paymentServices = new PaymentServices();
			String approvalLink = paymentServices.authorizePayment(orderDetail);
			FacesContext.getCurrentInstance().getExternalContext().redirect(approvalLink);
		} catch (PayPalRESTException ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public String getPlan() {
		return plan;
	}

	public void setPlan(String plan) {
		this.plan = plan;
	}

	public String redirectWithPlan(String planValue) {
		this.setPlan(planValue);
		return "checkout.xhtml";
	}
}
